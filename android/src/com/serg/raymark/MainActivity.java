package com.serg.raymark;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.*;

//import com.google.android.gms.ads.*;

public class MainActivity extends Activity {
    private WebView wv_;
    private WebViewInterface wi_;
    private DrawView dv_;
    private Timer timer_;
    private UpdateTask update_task_;
	private String device_name_;
	
	private final String SITE_URL = "http://194.87.234.91";
    
	private String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        wv_ = new WebView(this);
        wi_ = new WebViewInterface(this);
        dv_ = new DrawView(this);

        wv_.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        wv_.setLongClickable(false);
        wv_.setHapticFeedbackEnabled(false);

        wv_.getSettings().setJavaScriptEnabled(true);
        wv_.addJavascriptInterface(wi_, "MainApp");
        wv_.loadUrl("file:///android_asset/resources/index.html");
		
		if (android.os.Build.VERSION.SDK_INT >= 19) {
			wv_.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		} else {
			wv_.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}
		
		wv_.setOverScrollMode(View.OVER_SCROLL_NEVER);
		
        setContentView(wv_);
		
		String manufacturer = android.os.Build.MANUFACTURER;
		String model = android.os.Build.MODEL;
		if (model.startsWith(manufacturer)) {
			device_name_ = capitalize(model);
		} else {
			device_name_ = capitalize(manufacturer) + " " + model;
		}
    }

    public class WebViewInterface {
        private Activity activity_;
        private int test_result_;
		private int info_click_counter_ = 0;

        WebViewInterface(Activity c) {
            activity_ = c;
			
			SharedPreferences shared_pref = activity_.getPreferences(Context.MODE_PRIVATE);
			test_result_ = shared_pref.getInt("LastResult", 0);
        }
        
        void set_test_result(int result) {
            test_result_ = result;
			
			SharedPreferences shared_pref = activity_.getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = shared_pref.edit();
			editor.putInt("LastResult", test_result_);
			editor.commit();
        }

        @JavascriptInterface
        public void StartTest() {
            activity_.runOnUiThread(new Runnable() {
                public void run() {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    
                    wv_.loadUrl("file:///android_asset/resources/loading.html");
                    
                    LibJNI.Destroy();
                    LibJNI.Init(1280, 720, activity_.getAssets());
                    LibJNI.AsyncLoadScene("scenes/inter_android.json");
                    
                    timer_ = new Timer();
                    update_task_ = new UpdateTask(activity_);
                    timer_.schedule(update_task_, 0, 1000);
                }
            });
        }
        
        @JavascriptInterface
        public int GetLastResult() {
            return test_result_;
        }
		
		@JavascriptInterface
		public String GetDeviceName() {
			return device_name_;
		}
		
		@JavascriptInterface
		public String GetResultsTableJson() {
			try {
				URL url = new URL(SITE_URL + "/results_raw");
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setRequestMethod("GET");
				//http.setDoOutput(true);
				http.setConnectTimeout(5000);
				
				int rc = http.getResponseCode();
				
				BufferedReader rd = new BufferedReader(new InputStreamReader(http.getInputStream()));
				
				StringBuilder result = new StringBuilder();
				
			
				String line;
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
				rd.close();
				
				return result.toString();
			} catch(Exception e) {
				return "";
			}
		}
		
		@JavascriptInterface
		public void AsyncUpdateResults(String params) {
			try {
				URL url = new URL(SITE_URL + "/results_raw" + (params.isEmpty() ? "" : ("?" + params)));
				new HTTPRequestTask("OnResultsUpdate").execute(url);
			} catch(Exception e) {}
		}
		
		@JavascriptInterface
		public void AsyncQueryCPUName(String device_name) {
			try {
				URL url = new URL(SITE_URL + "/get_cpu?device=" + device_name);
				new HTTPRequestTask("OnCpuUpdate").execute(url);
			} catch(Exception e) {}
		}
		
		@JavascriptInterface
		public void SearchDevice(String device_name) {
			Intent browser_intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/search?q=" + device_name));
			startActivity(browser_intent);
		}
		
		@JavascriptInterface
		public void ShowInfo() {
			info_click_counter_++;
			
			if (info_click_counter_ > 10) {
				final SpannableString s = new SpannableString("Thank you for using my app, it was created for learning purposes mostly. " +
															  "PathTracing lib used here is open source and can be found at my github page: https://github.com/MrApfel1994/ray. " +
															  "If you have any questions or propositions for app improvement, you can always contact me: yablokovsergey94@gmail.com");
				Linkify.addLinks(s, Linkify.ALL);
				
				final AlertDialog d = new AlertDialog.Builder(activity_)
											.setTitle("Info")
											.setMessage(s)
											.setPositiveButton("OK", null)
											.setCancelable(false)
											.create();
				d.show();
				
				((TextView)d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
				
				info_click_counter_ = 0;
			}
		}
		
		private class HTTPRequestTask extends AsyncTask<URL, Void, String> {
			private String js_callback_;
			
			HTTPRequestTask(String js_callback) {
				js_callback_ = js_callback;
			}
			
			@Override
			protected String doInBackground(URL... urls) {
				try {
					URL url = urls[0];
					HttpURLConnection http = (HttpURLConnection)url.openConnection();
					http.setRequestMethod("GET");
					//http.setDoOutput(true);
					http.setConnectTimeout(5000);
					
					int rc = http.getResponseCode();
					
					BufferedReader rd = new BufferedReader(new InputStreamReader(http.getInputStream()));
					
					StringBuilder result = new StringBuilder();
					
					String line;
					while ((line = rd.readLine()) != null) {
						result.append(line);
					}
					rd.close();
					
					return result.toString();
				} catch(Exception e) {
					return "";
				}
			}
			
			@Override
			protected void onPostExecute(String result){
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
					wv_.evaluateJavascript(js_callback_ + "(" + result + ");", null);
				} else {
					wv_.loadUrl("javascript:" + js_callback_ + "(" + result + ");");
				}
			}
		}
    }
    
    class UpdateTask extends TimerTask {
        private Activity activity_;
        
        UpdateTask(Activity activity) {
            activity_ = activity;
        }
		
		private Map<String, String> GetCPUInfo() throws IOException {
			Map<String, String> output = new HashMap<> ();

			BufferedReader br = new BufferedReader (new FileReader ("/proc/cpuinfo"));

			String str;

			while ((str = br.readLine ()) != null) {
				String[] data = str.split (":");
				if (data.length > 1) {
					String key = data[0].trim ().replace (" ", "_");
					if (key.equals ("model_name")) key = "cpu_model";

					String value = data[1].trim ();

					if (key.equals ("cpu_model"))
					  value = value.replaceAll ("\\s+", " ");

					output.put (key, value);
				}
			}

			br.close ();

			return output;
		}
		
        @Override
        public void run() {
            int state = LibJNI.GetState();
            
            if (state == LibJNI.Loading_Started) {
                
            } else if (state == LibJNI.Loading_Finished) {
                activity_.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //wv_.loadUrl("file:///android_asset/resources/warmup.html");
						
						wv_.evaluateJavascript("ChangeText(\"Warmup\");", null);
                    }
                });
                
                LibJNI.AsyncWarmup(5);
            } else if (state == LibJNI.Warmup_Started) {
                
            } else if (state == LibJNI.Warmup_Finished) {
                activity_.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dv_.Clear();
                        setContentView(dv_);
                    }
                });
                
                timer_.cancel();
                timer_.purge();
                
                LibJNI.AsyncStartTest();

                timer_ = new Timer();
                update_task_ = new UpdateTask(activity_);
                timer_.schedule(update_task_, 0, 100);
            } else if (state == LibJNI.Test_Started) {
                final int[] updated_regions = new int[300];
                
                boolean is_finished = LibJNI.QueryTestStatus(updated_regions);
                
                int num_regions = 0;
                for (; num_regions < 300/5; num_regions++) {
                    if (updated_regions[num_regions * 5 + 2] == 0) break;
                }
                
                if (num_regions != 0) {
                    activity_.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dv_.UpdateRegions(updated_regions);
                            dv_.invalidate();
                        }
                    });
                }
            } else if (state == LibJNI.Test_Finished) {
                timer_.cancel();
                timer_.purge();
                
				int result = LibJNI.GetTestResult();
                wi_.set_test_result(result);
				
				String cpu_model = "unknown";
				
				try {
					Map<String, String> cpu_info = GetCPUInfo();
					if (cpu_info.containsKey("cpu_model")) {
						cpu_model = cpu_info.get("cpu_model");
					}
				} catch(Exception e) {
					Log.d("", "Cannot obtain CPU model!");
				}
				
				try {
					URL url = new URL(SITE_URL + "/results_raw");
					HttpURLConnection http = (HttpURLConnection)url.openConnection();
					http.setRequestMethod("POST");
					http.setDoOutput(true);
					http.setConnectTimeout(5000);
					
					String hash;
					
					{	// calc hash
						String salt1 = "gCHy7jH2w4";
						String salt2 = "rsx3z2SxIt";
					
						final MessageDigest digest = MessageDigest.getInstance("SHA-1");
						digest.update(salt1.getBytes("UTF-8"));
						digest.update(String.valueOf(result).getBytes("UTF-8"));
						digest.update(salt2.getBytes("UTF-8"));
						byte[] bytes = digest.digest();

						StringBuilder sb = new StringBuilder();
						for (byte b : bytes) {
							sb.append(String.format("%02x", b));
						}

						hash = sb.toString();
					}
					
					String out_data = "{\"device_name\": \"";
					out_data += device_name_;
					out_data += "\", \"cpu_name\": \"";
					out_data += cpu_model;
					out_data += "\", \"result\": ";
					out_data += String.valueOf(result);
					out_data += ", \"hash\": \"";
					out_data += hash;
					out_data += "\" }";
					
					byte[] out = out_data.getBytes(StandardCharsets.UTF_8);
					int length = out.length;

					http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
					http.setRequestProperty( "Content-Length", String.valueOf(length));
					try(OutputStream os = http.getOutputStream()) {
						os.write(out);
					}
					
					int rc = http.getResponseCode();
					
					Log.d("", String.valueOf(rc));
				} catch(Exception e) {
					Log.d("", "Cannot send results!");
				}
                
                activity_.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        
                        wv_.loadUrl("file:///android_asset/resources/index.html");
                        setContentView(wv_);
                    }
                });
            } else if (state == LibJNI.Error) {
                activity_.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        
                        wv_.loadUrl("file:///android_asset/resources/error.html");
                        setContentView(wv_);
                    }
                });
            }
        }
    }
    
    class DrawView extends View {
        private Bitmap bitmap_;
        private int[] updated_regions_;
        
        public DrawView(Context context) {
            super(context);
            bitmap_ = Bitmap.createBitmap(1280, 720, Bitmap.Config.ARGB_8888);
        }
        
        public void Clear() {
            int color = (255 & 0xff) << 24 | (0 & 0xff) << 16 | (255 & 0xff) << 8 | (255 & 0xff);
            bitmap_.eraseColor(0xff000000);
        }
         
        @Override
        protected void onDraw(Canvas canvas) {
            Rect clip = canvas.getClipBounds();
            
            float kx = getWidth() / 1280.0f, ky = getHeight() / 720.0f;
            
            float k = 1.0f;//(((float)getWidth()) / getHeight()) * (1280.0f / 720.0f);
            
            //canvas.drawBitmap(bitmap_, new Rect((int)(clip.left * kx), (int)(clip.top * ky),
            //                                  (int)(clip.right * kx), (int)(clip.bottom * ky)), clip, null);
                                                
            canvas.drawBitmap(bitmap_, new Rect(0, 0, 1280, 720), new Rect(clip.left, clip.top, clip.right, (int)(clip.bottom * k)), null);
            
            /*int num_regions = updated_regions_.length / 5;
            
            float kx = getWidth() / 1280.0f, ky = getHeight() / 720.0f;
            
            for (int i = 0; i < num_regions; i++) {
                int px = updated_regions_[i * 5 + 0], py = updated_regions_[i * 5 + 1],
                    sx = updated_regions_[i * 5 + 2], sy = updated_regions_[i * 5 + 3],
                    is_active = updated_regions_[i * 5 + 4];

                if (sx == 0) break;
                
                int _px = (int)(px * kx), _py = (int)(py * ky), _sx = (int)(sx * kx), _sy = (int)(sy * ky);
                
                canvas.drawBitmap(bitmap_, new Rect(px, py, sx, sy), new Rect(_px, _py, _sx, _sy), null);
            }*/
        }
        
        public void UpdateRegions(int[] updated_regions) {
            int num_regions = updated_regions.length / 5;
            
            for (int i = 0; i < num_regions; i++) {
                int px = updated_regions[i * 5 + 0], py = updated_regions[i * 5 + 1],
                    sx = updated_regions[i * 5 + 2], sy = updated_regions[i * 5 + 3],
                    is_active = updated_regions[i * 5 + 4];

                if (sx == 0) break;
                    
                int[] region_pixels = new int[sx * sy];
                LibJNI.FillPixelsRect(region_pixels, px, py, sx, sy);
                
                bitmap_.setPixels(region_pixels, 0, sx, px, py, sx, sy);
                
                for (int y = py; y < py + sy; y++) {
                    for (int x = px; x < px + sx; x++) {
                        if (is_active != 0 && (y == py || y == py + sy - 1 || 
                            ((x == px || x == px + sx - 1) && (y < py + 4 || y > py + sy - 5)))) {
                            bitmap_.setPixel(x, y, 0xffffffff);
                        }
                    }
                }
            }
        }
    }
}
