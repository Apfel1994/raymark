#include "MainView.h"

#include <memory>

#include <QtGui/QPainter>
#include <QtGui/QPaintEvent>

MainView::MainView(int w, int h) : w_(w), h_(h), pix_data_(new uchar[w * h * 3]) {
    Clear();
}

void MainView::Clear() {
    memset(&pix_data_[0], 0, w_ * h_ * 3);
}

void MainView::UpdateRegions(const float *pixels, int *regions, int num_regions) {
    for (int i = 0; i < num_regions; i++) {
        const int px = regions[i * 5 + 0], py = regions[i * 5 + 1],
                  sx = regions[i * 5 + 2], sy = regions[i * 5 + 3],
                  is_active = regions[i * 5 + 4];

        for (int y = py; y < py + sy; y++) {
            for (int x = px; x < px + sx; x++) {
                if (is_active && (y == py || y == py + sy - 1 || 
                    ((x == px || x == px + sx - 1) && (y < py + 4 || y > py + sy - 5)))) {
                    pix_data_[(w_ * y + x) * 3 + 0] = 255;
                    pix_data_[(w_ * y + x) * 3 + 1] = 255;
                    pix_data_[(w_ * y + x) * 3 + 2] = 255;
                } else {
                    pix_data_[(w_ * y + x) * 3 + 0] = (uchar)(255 * pixels[(w_ * y + x) * 4 + 0]);
                    pix_data_[(w_ * y + x) * 3 + 1] = (uchar)(255 * pixels[(w_ * y + x) * 4 + 1]);
                    pix_data_[(w_ * y + x) * 3 + 2] = (uchar)(255 * pixels[(w_ * y + x) * 4 + 2]);
                }
            }
        }

        repaint(QRect{ px, py, sx, sy });
    }
}

void MainView::paintEvent(QPaintEvent *ev) {
    const auto rect = ev->rect();

    QPainter painter(this);

    QImage img(pix_data_.get() + 3 * (rect.y() * w_ + rect.x()), rect.width(), rect.height(), w_ * 3, QImage::Format_RGB888);
    painter.drawImage(rect.x(), rect.y(), img);
}