#include "DesktopApp.h"

#include <QtCore/QCryptographicHash>
#include <QtCore/QEventLoop>
#include <QtGui/QDesktopServices>
#include <QtGui/QMessageBox>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QtWebKit/QWebHistory>

#include "MainView.h"
#include "WebView.h"

#include "moc_DesktopApp.cpp"

DesktopApp::DesktopApp(WebView &web_view, MainView &main_view, const char *site_url) 
    : web_view_(web_view), main_view_(main_view), site_url_(site_url), raymark_lib_("raymark_lib") {
    //web_view_.page()->mainFrame()->addToJavaScriptWindowObject("MainApp", this);
    QObject::connect(web_view_.page()->mainFrame(), SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(AttachJsObject()));

    if (!raymark_lib_.load()) {
        web_view_.load(QUrl("assets/resources/error.html"));
    } else {
        web_view_.load(QUrl("assets/resources/index.html"));

        p_init_ = (decltype(p_init_))raymark_lib_.resolve("Init");
        p_destroy_ = (decltype(p_destroy_))raymark_lib_.resolve("Destroy");
        p_get_state_ = (decltype(p_get_state_))raymark_lib_.resolve("GetState");
        p_get_error_ = (decltype(p_get_error_))raymark_lib_.resolve("GetError");
        p_async_load_scene_ = (decltype(p_async_load_scene_))raymark_lib_.resolve("AsyncLoadScene");
        p_async_warmup_ = (decltype(p_async_warmup_))raymark_lib_.resolve("AsyncWarmup");
        p_async_start_test_ = (decltype(p_async_start_test_))raymark_lib_.resolve("AsyncStartTest");
        p_query_test_status_ = (decltype(p_query_test_status_))raymark_lib_.resolve("QueryTestStatus");
        p_get_test_result_ = (decltype(p_get_test_result_))raymark_lib_.resolve("GetTestResult");

        if (!p_init_ || !p_destroy_ || !p_get_state_ || !p_get_error_ || !p_async_load_scene_ || !p_async_warmup_ ||
            !p_async_start_test_ || !p_query_test_status_ || !p_get_test_result_) {
            throw std::runtime_error("Failed to find functions.");
        }

        timer_ = new QTimer(this);
        QObject::connect(timer_, SIGNAL(timeout()), this, SLOT(Update()));

        auto *am = web_view_.page()->networkAccessManager();
        QObject::connect(am, SIGNAL(finished(QNetworkReply*)), SLOT(OnReply(QNetworkReply*)));
    }

    auto *am = web_view_.page()->networkAccessManager();
    
    device_name_ = "PC@Unknown";

    QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
    for (const auto &i : interfaces) {
        QString mac = i.hardwareAddress();
        if (!mac.isEmpty()) {
            device_name_ = "PC@" + mac;
            break;
        }
    }
}

DesktopApp::~DesktopApp() {
    p_destroy_();
}

void DesktopApp::StartTest() {
    web_view_.load(QUrl("assets/resources/loading.html"));

    p_destroy_();
    p_init_(1280, 720);
    p_async_load_scene_("assets/scenes/inter.json");

    timer_->start(1000);
}

int DesktopApp::GetLastResult() {
    return p_get_test_result_();
}

QString DesktopApp::GetDeviceName() {
    return device_name_;
}

QString DesktopApp::GetResultsTableJson() {
    auto *am = web_view_.page()->networkAccessManager();

    QNetworkRequest request(QUrl(site_url_ + "/results_raw"));
    QNetworkReply *reply = am->get(request);

    QEventLoop loop;
    loop.connect(am, SIGNAL(finished(QNetworkReply*)), SLOT(quit()));
    loop.exec();

    QByteArray data = reply->readAll();

    return QString::fromAscii(data.data(), data.length());
}

void DesktopApp::AsyncUpdateResults(QString params) {
    auto *am = web_view_.page()->networkAccessManager();

    QString request_str = site_url_ + "/results_raw";
    if (!params.isEmpty()) {
        request_str += "?";
        request_str += params;
    }

    QNetworkRequest request(QUrl{ request_str });
    QNetworkReply *reply = am->get(request);
}

void DesktopApp::AsyncQueryCPUName(QString device_name) {
    auto *am = web_view_.page()->networkAccessManager();

    QString request_str = site_url_ + "/get_cpu?device=" + device_name;

    QNetworkRequest request(QUrl{ request_str });
    QNetworkReply *reply = am->get(request);
}

void DesktopApp::SearchDevice(QString device_name) {
    QDesktopServices::openUrl(QUrl("http://www.google.com/search?q=" + device_name));
}

void DesktopApp::ShowInfo() {
    info_click_counter_++;

    if (info_click_counter_ > 10) {
        QMessageBox::about(nullptr, "Info",
                           "Thank you for using my app, it was created for learning purposes mostly. "
                           "PathTracing lib used here is open source and can be found at my github page: <a href='https://github.com/MrApfel1994/ray'>https://github.com/MrApfel1994/ray</a>. "
                           "If you have any questions or propositions for app improvement, you can always contact me: <a href=\"mailto:yablokovsergey94@gmail.com\">yablokovsergey94@gmail.com</a>");
        info_click_counter_ = 0;
    }
}

void DesktopApp::Update() {
    int state = p_get_state_();

    if (state == Loading_Started) {
    
    } else if (state == Loading_Finished) {
        QString eval_str = "ChangeText(%1);";
        web_view_.page()->mainFrame()->evaluateJavaScript(eval_str.arg("\"Warmup\""));
        p_async_warmup_(5);
    } else if (state == Warmup_Started) {
        
    } else if (state == Warmup_Finished) {
        web_view_.hide();
        main_view_.Clear();
        main_view_.show();
        timer_->stop();

        p_async_start_test_();
        timer_->start(100);
    } else if (state == Test_Started) {
        int is_finished;
        int updated_regions[300];
        int num_updated_regions = 300/5;
        const float *pixels = nullptr;
        p_query_test_status_(&is_finished, updated_regions, &num_updated_regions, (void **)&pixels);

        main_view_.UpdateRegions(pixels, updated_regions, num_updated_regions);
    } else if (state == Test_Finished) {
        auto *am = web_view_.page()->networkAccessManager();

        const char *salt1 = "gCHy7jH2w4";
        const char *salt2 = "rsx3z2SxIt";
        std::string result_str = std::to_string(GetLastResult());

        QCryptographicHash hash{QCryptographicHash::Sha1};
        hash.addData(salt1, strlen(salt1));
        hash.addData(result_str.c_str(), result_str.length());
        hash.addData(salt2, strlen(salt2));

        QString hash_str = hash.result().toHex();

        QNetworkRequest request(QUrl(site_url_ + "/results_raw"));
        QByteArray data = ("{\"device_name\": \"");
        data.append(device_name_);
        data.append("\", \"cpu_name\": \"cpu\", \"result\": ");
        data.append(QString::fromStdString(result_str));
        data.append(", \"hash\": \"");
        data.append(hash_str);
        data.append("\" }");

        request.setRawHeader("Content-Type", "application/json; charset=UTF-8");
        request.setRawHeader("Content-Length", QString::number(data.length()).toUtf8());

        am->post(request, data);

        QEventLoop loop;
        loop.connect(am, SIGNAL(finished(QNetworkReply*)), SLOT(quit()));
        loop.exec();

        web_view_.load(QUrl("assets/resources/index.html"));
        web_view_.show();
        main_view_.hide();

        timer_->stop();
    } else if (state == Error) {
        web_view_.load(QUrl("assets/resources/error.html"));
        timer_->stop();
    }
}

void DesktopApp::AttachJsObject() {
    web_view_.page()->mainFrame()->addToJavaScriptWindowObject("MainApp", this);
}

void DesktopApp::OnReply(QNetworkReply *reply) {
    auto req = reply->request();

    auto data = reply->readAll();

    auto url_str = req.url().toString();

    if (url_str.startsWith(site_url_ + "/results_raw")) {
        QString eval_str = "OnResultsUpdate(%1);";
        web_view_.page()->mainFrame()->evaluateJavaScript(eval_str.arg(QString::fromAscii(data.data(), data.length())));
    } else if (url_str.startsWith(site_url_ + "/get_cpu")) {
        QString eval_str = "OnCPUUpdate(%1);";
        web_view_.page()->mainFrame()->evaluateJavaScript(eval_str.arg(QString::fromAscii(data.data(), data.length())));
    }
}