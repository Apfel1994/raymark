/****************************************************************************
** Meta object code from reading C++ file 'DesktopApp.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "DesktopApp.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DesktopApp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DesktopApp[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      21,   11,   11,   11, 0x0a,
      44,   38,   11,   11, 0x0a,

 // methods: signature, parameters, type, tag, flags
      68,   11,   11,   11, 0x02,
      84,   11,   80,   11, 0x02,
     108,   11,  100,   11, 0x02,
     124,   11,  100,   11, 0x02,
     153,  146,   11,   11, 0x02,
     193,  181,   11,   11, 0x02,
     220,  181,   11,   11, 0x02,
     242,   11,   11,   11, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_DesktopApp[] = {
    "DesktopApp\0\0Update()\0AttachJsObject()\0"
    "reply\0OnReply(QNetworkReply*)\0StartTest()\0"
    "int\0GetLastResult()\0QString\0GetDeviceName()\0"
    "GetResultsTableJson()\0params\0"
    "AsyncUpdateResults(QString)\0device_name\0"
    "AsyncQueryCPUName(QString)\0"
    "SearchDevice(QString)\0ShowInfo()\0"
};

void DesktopApp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DesktopApp *_t = static_cast<DesktopApp *>(_o);
        switch (_id) {
        case 0: _t->Update(); break;
        case 1: _t->AttachJsObject(); break;
        case 2: _t->OnReply((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 3: _t->StartTest(); break;
        case 4: { int _r = _t->GetLastResult();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 5: { QString _r = _t->GetDeviceName();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 6: { QString _r = _t->GetResultsTableJson();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 7: _t->AsyncUpdateResults((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->AsyncQueryCPUName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->SearchDevice((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->ShowInfo(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DesktopApp::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DesktopApp::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DesktopApp,
      qt_meta_data_DesktopApp, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DesktopApp::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DesktopApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DesktopApp::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DesktopApp))
        return static_cast<void*>(const_cast< DesktopApp*>(this));
    return QObject::qt_metacast(_clname);
}

int DesktopApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
