#pragma once

#include <memory>

#include <QtGui/QWidget>

class MainView : public QWidget {
    int w_, h_;
    std::unique_ptr<uchar[]> pix_data_;
public:
    MainView(int w, int h);

    void Clear();

    void UpdateRegions(const float *pixels, int *regions, int num_regions);

    virtual void paintEvent(QPaintEvent *ev) override;
};