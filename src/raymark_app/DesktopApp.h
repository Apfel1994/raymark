#pragma once

#include <QtCore/QObject>
#include <QtCore/QLibrary>
#include <QtCore/QTimer>

#include <raymark_lib/exports.h>

class MainView;
class WebView;

class QNetworkReply;

class DesktopApp : public QObject {
    Q_OBJECT

    WebView &web_view_;
    MainView &main_view_;
    QString site_url_;

    QLibrary raymark_lib_;

    QTimer *timer_;

    PFN_INIT p_init_;
    PFN_DESTROY p_destroy_;
    PFN_GET_STATE p_get_state_;
    PFN_GET_ERROR p_get_error_;

    PFN_ASYNC_LOADSCENE p_async_load_scene_;
    PFN_ASYNC_WARMUP p_async_warmup_;
    PFN_ASYNC_START_TEST p_async_start_test_;
    PFN_QUERY_TEST_STATUS p_query_test_status_;
    PFN_GET_TEST_RESULT p_get_test_result_;

    QString device_name_;

    int info_click_counter_ = 0;
public:
    DesktopApp(WebView &web_view, MainView &main_view, const char *site_url);
    ~DesktopApp();

    Q_INVOKABLE void StartTest();
    Q_INVOKABLE int GetLastResult();
    Q_INVOKABLE QString GetDeviceName();
    Q_INVOKABLE QString GetResultsTableJson();
    Q_INVOKABLE void AsyncUpdateResults(QString params);
    Q_INVOKABLE void AsyncQueryCPUName(QString device_name);
    Q_INVOKABLE void SearchDevice(QString device_name);
    Q_INVOKABLE void ShowInfo();
public slots:
    void Update();
    void AttachJsObject();
    void OnReply(QNetworkReply *reply);
};