
#include <QtGui/QApplication>
#include <QtGui/QVBoxLayout>

#include <fstream>
#include <iostream>
#include <thread>

#include "DesktopApp.h"
#include "MainView.h"
#include "WebView.h"

const char *SITE_URL = "http://194.87.234.91";
//const char *SITE_URL = "http://10.0.0.30:8080";
//const char *SITE_URL = "http://127.0.0.1:8080";

int main(int argc, char *argv[]) {
    QApplication::setAttribute(Qt::AA_X11InitThreads);

    QApplication qt_app(argc, argv);

    QWidget main_widget;
    QVBoxLayout layout;
    layout.setMargin(0);

    main_widget.setLayout(&layout);

    WebView web_view;

    MainView main_view(1280, 720);
    main_view.hide();

    layout.addWidget(&web_view);
    layout.addWidget(&main_view);

    main_widget.setFixedSize(1280, 720);
    main_widget.show();

    DesktopApp desk_app(web_view, main_view, SITE_URL);

#if defined(QT_OPENSSL) && !defined(QT_NO_OPENSSL)
    web_view.connect(web_view.page()->networkAccessManager(),
                     SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> &)),
                     SLOT(sslErrorHandler(QNetworkReply*, const QList<QSslError> &)));
#endif

    return qt_app.exec();
}

#ifdef _WIN32
#include <Windows.h>

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, INT nCmdShow) {
    //int argc = 0;
    //char **argv = CommandLineToArgvW(lpCmdLine, &argc);
    return main(__argc, __argv);
}
#endif
