#include "exports.h"

#include <cmath>

#include <fstream>
#include <sstream>

#include <Sys/AssetFile.h>
#include <Sys/Json.h>
#include <Sys/Log.h>
#include <Sys/ThreadPool.h>

#include <Ray/RendererFactory.h>

#include "Load.h"

namespace {
    const int BUCKET_SIZE = 48;
    const int PASSES = 1;
    const int SPP_PORTION = 64;

    // From wikipedia page about Hilbert curve

    void rot(int n, int *x, int *y, int rx, int ry) {
        if (ry == 0) {
            if (rx == 1) {
                *x = n-1 - *x;
                *y = n-1 - *y;
            }

            //Swap x and y
            int t  = *x;
            *x = *y;
            *y = t;
        }
    }

    void d2xy(int n, int d, int *x, int *y) {
        int rx, ry, s, t = d;
        *x = *y = 0;
        for (s = 1; s < n; s *= 2) {
            rx = 1 & (t / 2);
            ry = 1 & (t ^ rx);
            rot(s, x, y, rx, ry);
            *x += s * rx;
            *y += s * ry;
            t /= 4;
        }
    }
}

struct {
    std::shared_ptr<Sys::ThreadPool> threads;
    std::shared_ptr<Ray::RendererBase> renderer;
    std::shared_ptr<Ray::SceneBase> scene;
    std::vector<Ray::RegionContext> region_contexts;
    std::vector<std::future<void>> events;

    std::mutex state_mtx;
    int state = -1;
    const char *error_str = nullptr;

    std::mutex timers_mutex;
    std::chrono::high_resolution_clock::time_point start_time, end_time;

    std::atomic_bool test_aborted;
    std::unique_ptr<std::atomic_bool[]> is_tile_active;
    std::unique_ptr<std::atomic_bool[]> is_tile_updated;

    std::atomic_bool warmup_done;
    std::atomic_int num_workers_ready;
    
    int last_not_ready_event;
} g_state;

extern "C" void Init(int w, int h) {
    auto num_threads = std::max(std::thread::hardware_concurrency(), 1u);
    g_state.threads = std::make_shared<Sys::ThreadPool>(num_threads);

    Ray::settings_t s;
    s.w = w;
    s.h = h;
    g_state.renderer = Ray::CreateRenderer(s, Ray::RendererAVX2 | Ray::RendererAVX | Ray::RendererSSE2 | Ray::RendererNEON | Ray::RendererRef);
}

extern "C" void Destroy() {
    g_state.test_aborted = true;

    for (size_t i = 0; i < g_state.events.size(); i++) {
        g_state.events[i].wait();
    }
    g_state.events.clear();
    g_state.region_contexts.clear();

    g_state.start_time = {};
    g_state.end_time = {};

    g_state.is_tile_active = nullptr;
    g_state.is_tile_updated = nullptr;

    g_state.threads = nullptr;
    g_state.scene = nullptr;
    g_state.renderer = nullptr;
}

extern "C" int GetState() {
    std::lock_guard<std::mutex> _(g_state.state_mtx);
    return g_state.state;
}

extern "C" void GetError(char out_str[128]) {
    std::lock_guard<std::mutex> _(g_state.state_mtx);
    strcpy(out_str, g_state.error_str);
}

extern "C" void AsyncLoadScene(const char *name) {
    {
        std::lock_guard<std::mutex> _(g_state.state_mtx);
        g_state.state = Loading_Started;
    }

    std::string _name = name;
    g_state.threads->enqueue([_name]() {
        JsObject js_scene;

        try {
            Sys::AssetFile in_file(_name);
            size_t in_file_size = in_file.size();

            std::string in_file_data;
            in_file_data.resize(in_file_size);
            in_file.Read((char *)in_file_data.data(), in_file_size);

            std::stringstream ss(in_file_data);
            
            if (!js_scene.Read(ss)) {
                std::lock_guard<std::mutex> _(g_state.state_mtx);
                g_state.state = Error;
                g_state.error_str = "Failed to load scene file!";
                return;
            }

            g_state.scene = LoadScene(g_state.renderer.get(), js_scene);
        } catch(...) {
            std::lock_guard<std::mutex> _(g_state.state_mtx);
            g_state.state = Error;
            g_state.error_str = "Failed to load scene file!";
            return;
        }

        {
            std::lock_guard<std::mutex> _(g_state.state_mtx);
            g_state.state = Loading_Finished;
        }
    });
}

extern "C" void AsyncWarmup(int time_limit_s) {
    {
        std::lock_guard<std::mutex> _(g_state.state_mtx);
        g_state.state = Warmup_Started;
    }

    int num_workers = g_state.threads->num_workers();

    g_state.warmup_done = false;
    g_state.num_workers_ready = 0;

    for (int i = 0; i < num_workers; i++) {
        g_state.threads->enqueue([time_limit_s, num_workers]() {
            double mean_us = 0, deviation = 999999;
            int iterations = 0;

            bool signaled_ready = false;

            auto warmup_start = std::chrono::high_resolution_clock::now();

            while (!g_state.warmup_done) {
                iterations++;

                auto t1 = std::chrono::high_resolution_clock::now();

                volatile double f = 12;
                for (size_t i = 0; i < 1000000; i++) f += std::sqrt(f);

                auto t2 = std::chrono::high_resolution_clock::now();

                auto dt = std::chrono::duration_cast<std::chrono::duration<double, std::micro>>(t2 - t1);

                double k = 1.0 / iterations;

                deviation = dt.count() - mean_us;
                mean_us += deviation * k;

                std::chrono::duration<double, std::milli> time_since_start = std::chrono::high_resolution_clock::now() - warmup_start;
                if ((std::abs(deviation) < 10.0 || time_since_start.count() > 1000.0 * time_limit_s) && !signaled_ready) {
                    int num_ready = g_state.num_workers_ready.fetch_add(1);
                    signaled_ready = true;

                    if (num_ready + 1 == num_workers) {
                        std::lock_guard<std::mutex> _(g_state.state_mtx);
                        g_state.state = Warmup_Finished;
                    }
                }
            }
        });
    }

    {
        //std::lock_guard<std::mutex> _(g_state.state_mtx);
        //g_state.state_str = "warmup_finished";
    }
}

extern "C" void AsyncStartTest() {
    {
        std::lock_guard<std::mutex> _(g_state.state_mtx);
        g_state.state = Test_Started;
    }

    const auto rt = g_state.renderer->type();
    const auto sz = g_state.renderer->size();

    int resx = sz.first / BUCKET_SIZE + (sz.first % BUCKET_SIZE != 0);
    int resy = sz.second / BUCKET_SIZE + (sz.second % BUCKET_SIZE != 0);

    int res =  std::max(resx, resy);

    // round up to next power of two
    res--;
    res |= res >> 1;
    res |= res >> 2;
    res |= res >> 4;
    res |= res >> 8;
    res |= res >> 16;
    res++;

    for (int i = 0; i < res * res; i++) {
        int x, y;

        d2xy(res, i, &x, &y);

        if (x > resx - 1 || y > resy - 1) continue;

        x *= BUCKET_SIZE;
        y *= BUCKET_SIZE;

        auto rect = Ray::rect_t{ x, y,
                std::min(sz.first - x, BUCKET_SIZE),
                std::min(sz.second - y, BUCKET_SIZE) };

        g_state.region_contexts.emplace_back(rect);
    }

    g_state.test_aborted = false;
    g_state.is_tile_active = std::unique_ptr<std::atomic_bool[]>{ new std::atomic_bool[g_state.region_contexts.size()] };
    g_state.is_tile_updated = std::unique_ptr<std::atomic_bool[]>{ new std::atomic_bool[g_state.region_contexts.size()] };
    for (int i = 0; i < (int)g_state.region_contexts.size(); i++) {
        g_state.is_tile_active[i] = false;
        g_state.is_tile_updated[i] = false;
    }

    auto render_job = [](int i, int m) {
        if (g_state.test_aborted) return;

        {
            auto t = std::chrono::high_resolution_clock::now();

            std::lock_guard<std::mutex> _(g_state.timers_mutex);
            if (g_state.start_time == std::chrono::high_resolution_clock::time_point{}) {
                g_state.start_time = t;
            }
        }

        g_state.is_tile_updated[i] = true;
        g_state.is_tile_active[i] = true;
        for (int j = 0; j < SPP_PORTION * m; j++) {
            if (g_state.test_aborted) return;
            g_state.renderer->RenderScene(g_state.scene, g_state.region_contexts[i]);
        }
        g_state.is_tile_active[i] = false;

        {
            auto t = std::chrono::high_resolution_clock::now();

            std::lock_guard<std::mutex> _(g_state.timers_mutex);
            g_state.end_time = t;
        }
    };

    g_state.last_not_ready_event = 0;
    
    for (int s = 0; s < PASSES; s++) {
        for (int i = 0; i < (int)g_state.region_contexts.size(); i++) {
            g_state.events.push_back(g_state.threads->enqueue(render_job, i, (1 << s)));
        }
    }

    g_state.warmup_done = true;
}

extern "C" void QueryTestStatus(int *is_finished, int *updated_regions, int *num_updated_regions, void **pixels) {
    bool finished = false;

    if (is_finished) {
        finished = true;
        
        for (size_t i = g_state.last_not_ready_event; i < g_state.events.size(); i++) {
            g_state.last_not_ready_event = i;
            if (g_state.events[i].wait_for(std::chrono::milliseconds(5)) != std::future_status::ready) {
                finished = false;
                break;
            }
        }

        *is_finished = finished ? 1 : 0;
    }

    if (num_updated_regions) {
        int max_updated_regions = (*num_updated_regions);
        (*num_updated_regions) = 0;

        for (int i = 0; i < (int)g_state.region_contexts.size(); i++) {
            if (!g_state.is_tile_updated[i]) continue;

            bool active = true;

            if (!g_state.is_tile_active[i]) {
                g_state.is_tile_updated[i] = false;
                active = false;
            }

            if (*num_updated_regions >= max_updated_regions) break;

            updated_regions[(*num_updated_regions) * 5 + 0] = g_state.region_contexts[i].rect().x;
            updated_regions[(*num_updated_regions) * 5 + 1] = g_state.region_contexts[i].rect().y;
            updated_regions[(*num_updated_regions) * 5 + 2] = g_state.region_contexts[i].rect().w;
            updated_regions[(*num_updated_regions) * 5 + 3] = g_state.region_contexts[i].rect().h;
            updated_regions[(*num_updated_regions) * 5 + 4] = active ? 1 : 0;
            (*num_updated_regions)++;
        }
    }

    if (pixels) *pixels = (void *)g_state.renderer->get_pixels_ref();

    if (finished) {
        std::lock_guard<std::mutex> _(g_state.state_mtx);
        g_state.state = Test_Finished;
    }
}

extern "C" int GetTestResult() {
    auto dt = std::chrono::duration<double>{ g_state.end_time - g_state.start_time };
    if (dt.count() != 0) {
        return (int)std::round(1000000.0 / dt.count());
    } else {
        return 0;
    }
}
