#include "com_serg_raymark_LibJNI.h"

#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include <Sys/AssetFile.h>

#include "exports.h"

extern "C" JNIEXPORT void JNICALL Java_com_serg_raymark_LibJNI_Init(JNIEnv *env, jclass, jint w, jint h, jobject am) {
    AAssetManager *asset_mgr = AAssetManager_fromJava(env, am);
    Sys::AssetFile::InitAssetManager(asset_mgr);
    Init((int)w, (int)h);
}

extern "C" JNIEXPORT void JNICALL Java_com_serg_raymark_LibJNI_Destroy(JNIEnv *env, jclass obj) {
    Destroy();
}

extern "C" JNIEXPORT jint JNICALL Java_com_serg_raymark_LibJNI_GetState(JNIEnv *, jclass) {
    return (jint)GetState();
}

extern "C" JNIEXPORT void JNICALL Java_com_serg_raymark_LibJNI_AsyncLoadScene(JNIEnv *env, jclass, jstring jname) {
    const char *name = env->GetStringUTFChars(jname, 0);

    AsyncLoadScene(name);

    env->ReleaseStringUTFChars(jname, name);
}

extern "C" JNIEXPORT void JNICALL Java_com_serg_raymark_LibJNI_AsyncWarmup(JNIEnv *, jclass, jint time_limit_s) {
    AsyncWarmup((int)time_limit_s);
}

extern "C" JNIEXPORT void JNICALL Java_com_serg_raymark_LibJNI_AsyncStartTest(JNIEnv *, jclass) {
    AsyncStartTest();
}

extern "C" JNIEXPORT jboolean JNICALL Java_com_serg_raymark_LibJNI_QueryTestStatus(JNIEnv *env, jclass, jintArray jupdated_regions) {
    jsize len = env->GetArrayLength(jupdated_regions);
    if (len < 300) return (jboolean)0;
    
    int is_finished;
    int updated_regions[300];
    int num_updated_regions = 300/5;
    const float *pixels = nullptr;
    QueryTestStatus(&is_finished, updated_regions, &num_updated_regions, (void **)&pixels);
    
    jint *regions = env->GetIntArrayElements(jupdated_regions, 0);
    
    for (int i = 0; i < num_updated_regions; i++) {
        regions[i * 5 + 0] = (jint)updated_regions[i * 5 + 0];
        regions[i * 5 + 1] = (jint)updated_regions[i * 5 + 1];
        regions[i * 5 + 2] = (jint)updated_regions[i * 5 + 2];
        regions[i * 5 + 3] = (jint)updated_regions[i * 5 + 3];
        regions[i * 5 + 4] = (jint)updated_regions[i * 5 + 4];
    }
    
    env->ReleaseIntArrayElements(jupdated_regions, regions, 0);
    
    return (jboolean)is_finished;
}

extern "C" JNIEXPORT void JNICALL Java_com_serg_raymark_LibJNI_FillPixelsRect(JNIEnv *env, jclass, jintArray jpixels, jint x, jint y, jint w, jint h) {
    jint *pixels = env->GetIntArrayElements(jpixels, 0);
    
    const float *fpixels = nullptr;
    QueryTestStatus(nullptr, nullptr, nullptr, (void **)&fpixels);
    
    for (int j = y; j < y + h; j++) {
        for (int i = x; i < x + w; i++) {
            int r = (int)(fpixels[4 * (j * 1280 + i) + 0] * 255);
            int g = (int)(fpixels[4 * (j * 1280 + i) + 1] * 255);
            int b = (int)(fpixels[4 * (j * 1280 + i) + 2] * 255);
            
            int color = (255 & 0xff) << 24 | (r & 0xff) << 16 | (g & 0xff) << 8 | (b & 0xff);
            
            pixels[(j - y) * w + (i - x)] = (jint)color;
        }
    }
    
    env->ReleaseIntArrayElements(jpixels, pixels, 0);
}

extern "C" JNIEXPORT jint JNICALL Java_com_serg_raymark_LibJNI_GetTestResult(JNIEnv *, jclass) {
    return (jint)GetTestResult();
}
