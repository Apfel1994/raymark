#pragma once

#include <Sys/DynLib.h>

extern "C" {
    enum e_state {
        Error = -1,
        Loading_Started,
        Loading_Finished,
        Warmup_Started,
        Warmup_Finished,
        Test_Started,
        Test_Finished,
    };

    DLL_EXPORT void Init(int w, int h);
    DLL_EXPORT void Destroy();
    DLL_EXPORT int GetState();
    DLL_EXPORT void GetError(char out_str[128]);
    DLL_EXPORT void AsyncLoadScene(const char *name);
    DLL_EXPORT void AsyncWarmup(int time_limit_s);
    DLL_EXPORT void AsyncStartTest();
    DLL_EXPORT void QueryTestStatus(int *is_finished, int *updated_regions, int *num_updated_regions, void **pixels);
    DLL_EXPORT int GetTestResult();

    using PFN_INIT = decltype(&Init);
    using PFN_DESTROY = decltype(&Destroy);
    using PFN_GET_STATE = decltype(&GetState);
    using PFN_GET_ERROR = decltype(&GetError);
    using PFN_ASYNC_LOADSCENE = decltype(&AsyncLoadScene);
    using PFN_ASYNC_WARMUP = decltype(&AsyncWarmup);
    using PFN_ASYNC_START_TEST = decltype(&AsyncStartTest);
    using PFN_QUERY_TEST_STATUS = decltype(&QueryTestStatus);
    using PFN_GET_TEST_RESULT = decltype(&GetTestResult);
}
